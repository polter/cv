Pavel Artsishevskii CV
======================

Based on [PlushCV](https://github.com/sansquoi/PlushCV) template.

All fonts used are free and open source.

Latest PDFs are available in `CI/CD` -> `Artifacts`. 

## Dependencies

Compiles only with **XeTeX** and required **BibTex** for compiling publications and the .bib filetype.

## License

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
